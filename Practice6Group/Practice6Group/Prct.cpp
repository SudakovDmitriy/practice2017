#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>

#define MaxLength  100
#define MaxStudentCount  20

// ��������� � ������� �������� ������ ��������� � �� ������
typedef struct {
  char FIO[MaxLength];
  int Result[5];
} structStudent;

// ����������� ������ 20 �������� ��������� 
structStudent Group[MaxStudentCount];

// ���������� ��������� (�� ����� MaxStudenCount)
int StudentCount = 0;

int ReadFile(void);
void RTrim(char * AString);
int WaitCommand(void);
void ListStudent(void);
void List5(void);
void List3(void);
void List2(void);

//� ������� �������� ���� , ����������� �� �� ������� ������ ���. ������ ������� , ������� ���� ��������� ���� �� �����
int ReadFile(void)
{
	// ��������� �� ���� � ������� 
	FILE * TxtFile;
	TxtFile = fopen("���������.txt", "r");

	// ��������� ������� ���� ��� ��� 
	if (TxtFile == NULL)
	{
		// ���� ��������� �� ���� ������� - ������� 
		printf("������ �������� �����!\n");
		return 0;
	}

	char
		String[MaxLength + 20],
		Sep[10] = ";",
		*Substr,
		*Pos;

	int FieldNum;

	StudentCount = 0;

	// ������ ��������� ���� �� ������ �� ����� ����� 
	while (!feof(TxtFile))
	{
		// ������� ������� ������ �� ������.����� 
		if (fgets(String, MaxLength, TxtFile))
		{
			// ��������� ������ �� ������������ � ��������� � ������ 
			FieldNum = 0;

			Substr = String;
			for (;;)
			{
				Pos = strchr(Substr, ';');
				if (!Pos) break;
				*Pos = '\0';

				if (FieldNum == 0)
				{
					strcpy(Group[StudentCount].FIO, Substr);
				}
				else
				{
					Group[StudentCount].Result[FieldNum - 1] = atoi(Substr);
				}

				FieldNum++;
				Substr = Pos + 1;
			}

			// ������� ���������� ��������� 
			StudentCount++;
		}
	}
	fclose(TxtFile);
	return 1;
}

int WaitCommand(void)
{
  printf("��������� ��������:\n");
  int i = 0;
  printf(" 1. ������� ������ ���������\n");
  printf(" 2. ������� ������ ���������, � ������� ��� �� ����\n");
  printf(" 3. ������� ������ ���������, � ������� ���� ���� �� ���� ������\n");
  printf(" 4. ������� ������ ���������, � ������� ���� ���� ������\n");
  printf(" 0. ����� �� ���������\n");
  printf("\n");


  int Choice;
  do
  {
    Choice = -1;
    printf("����������, �������� ���� �����: ");
    scanf_s("%d", &Choice);
  } while (Choice < 0 || Choice > 4);

  getchar();
  printf("\n\n");
  return Choice;
}

void ListStudent(void)
{
  printf("\n");

  for (int i = 0; i < StudentCount; i++)
  {
    printf("%s: %d, %d, %d, %d, %d\n", Group[i].FIO, Group[i].Result[0], Group[i].Result[1], Group[i].Result[2], Group[i].Result[3], Group[i].Result[4]);
  }
  printf("\n");
}

void List5(void)
{
  //������ ���������, ������� ����� ��� �������� ������ �� 5;
  printf("\n");

  for (int i = 0; i < StudentCount; i++)
  {
    if (Group[i].Result[0] == 5 &&
      Group[i].Result[1] == 5 &&
      Group[i].Result[2] == 5 &&
      Group[i].Result[3] == 5 &&
      Group[i].Result[4] == 5)
    {
      printf("%s: %d, %d, %d, %d, %d\n", Group[i].FIO, Group[i].Result[0], Group[i].Result[1], Group[i].Result[2], Group[i].Result[3], Group[i].Result[4]);
    }
  }
  printf("\n");
}

void List3(void)
{
  //������ ���������, ������� ����� ���� - �� ���� ������ �� ���������;
  printf("\n");

  for (int i = 0; i < StudentCount; i++)
  {
    if (Group[i].Result[0] == 3 ||
      Group[i].Result[1] == 3 ||
      Group[i].Result[2] == 3 ||
      Group[i].Result[3] == 3 ||
      Group[i].Result[4] == 3)
    {
      printf("%s: %d, %d, %d, %d, %d\n", Group[i].FIO, Group[i].Result[0], Group[i].Result[1], Group[i].Result[2], Group[i].Result[3], Group[i].Result[4]);
    }
  }
  printf("\n");
}
int main(int argc, char * argv[])
{
	setlocale(LC_CTYPE, "ru");

	// ��������� ������ ������ � ������ 
	ReadFile();

	// ����������� ���� �������� ������� 
	for (int Command;;)
	{
		// �������� ������� 
		Command = WaitCommand();
		// ��������� �� ������� 
		switch (Command)
		{
		case 0: exit(0); break;
		case 1: ListStudent(); break;
		case 2: List5(); break;
		case 3: List3(); break;
		case 4: List2(); break;
		}
	}
}
void RTrim(char * AString)
{
	int i;

	for (i = strlen(AString) - 1; i >= 0; i--)
	{
		if (AString[i] != ' ' && AString[i] != '\t' && AString[i] != '\n')
		{
			break;
		}
	}
	AString[i + 1] = '\0';
}
void List2(void)
{
	//������ ���������, � ������� ���� ������.���� �������, ����� ����� ��� ���� ������, �� ����������� �� ������. 
	int Count2;

	for (int i = 0; i < StudentCount; i++)
	{
		Count2 = 0;

		if (Group[i].Result[0] == 2) Count2++;
		if (Group[i].Result[1] == 2) Count2++;
		if (Group[i].Result[2] == 2) Count2++;
		if (Group[i].Result[3] == 2) Count2++;
		if (Group[i].Result[4] == 2) Count2++;

		if (Count2 == 1)
		{
			printf("%s: %d, %d, %d, %d, %d\n", Group[i].FIO, Group[i].Result[0], Group[i].Result[1], Group[i].Result[2], Group[i].Result[3], Group[i].Result[4]);
		}
	}
	printf("\n");
}
